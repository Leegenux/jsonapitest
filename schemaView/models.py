from django.db import models


OCCUPATIONS = ('doctor', 'teacher', 'worker', 'programmer')

class TestModel(models.Model):
    class Meta:
        ordering = ('time_created', )

    time_created = models.DateTimeField(auto_created=True)
    name = models.CharField(max_length=32, )
    age = models.IntegerField(null=False)
    occupation = models.CharField(max_length=64)
    description = models.TextField()

