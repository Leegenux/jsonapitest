from rest_framework_json_api import serializers
from .models import TestModel


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = ('name', 'age', 'occupation', 'description')