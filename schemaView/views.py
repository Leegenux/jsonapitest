from django.shortcuts import render
from rest_framework import generics
from .serializers import TestSerializer
from .models import TestModel
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
class TestView(generics.GenericAPIView):
    """
    Current user's identity endpoint.

    GET /me
    """
    resource_name = 'users'
    serializer_class = TestSerializer
    permission_classes = (AllowAny, )

    def get(self):
        models = TestModel.objects.all()
        serializer = TestSerializer(models, many=True)  # 处理多个
        return Response(serializer.data)

    def post(self, request):
        serializer = TestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_created)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

