# JsonAPITest

## Integration of JSON API open source project

Two crucial steps forward richer json docs

* Copy and paste the highly recommended `REST_FRAMEWORK` configuration in `settings.py`, and make changes if neccessary.
[Recommended Configuration](https://django-rest-framework-json-api.readthedocs.io/en/stable/usage.html#configuration)

* Use `rest_framework_json_api.serializers` rather than `rest_framework.serializers`, because API documentation heavily relies on serializers.

## How to provide more information for schema-autogeneration

As stated in the official REST documents, `GenericAPIView` and its subclasses provides more detailed API docs than `APIView` (and its subclasses) in terms of schema-generation. Though you can manually get those information for `APIView`, but it's more work and not recommended.

And note that :
1. Function based views are harder for documentation-generation, so avoid writing them. 
2. You can specifically silence some of the schemas in autoschema by setting `schema = None` in view classes.
3. Permission levels helps control how much API information one can put hands on.

[GenericAPIView Reference](https://www.django-rest-framework.org/api-guide/generic-views/ 'click to jump')

